iterm-tmux-mailcap
==================

## About

`iterm-tmux-mailcap` is a small and not that smart script aiming to open attachments according to the `.mailcap` file. In essence, it's just a wrapper around [imgcat](https://www.iterm2.com/documentation-images.html) (for displaying images in plain terminal) and Google Chrome (for displaying everything else, including images if in [tmux](https://github.com/tmux/tmux/wiki)). Understands [Imgur](https://en.wikipedia.org/wiki/Imgur) albums (not that it is really necessary nowadays).

## Usage

A typical entry in `.mailcap` file looks like this:

    image/* ; /Users/s/usr/local/scripts/iterm-tmux-mailcap %s; needsterminal

## Disclaimer

Unsupported. Untested. Boring. Use -- and tweak -- [view_attachment](https://bitbucket.org/sjl/dotfiles/src/tip/mutt/view_attachment.sh) instead.
